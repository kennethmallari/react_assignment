import Product from './Product';

export default function Productlist(props) { // this function will receive props onProductRemoved
    console.log("props on product list", props);
    return (
        <div>
            {props.products.map((product, index) =>
                <Product key={index} product={product} onProductRemoved={props.onProductRemoved} />   /* Creating onProductRemoved props and using onProductRemoved function */
            )}
        </div>
    )
}