import { Link } from 'react-router-dom';

export default function Nav() {
    return <div>
        <nav class="navbar navbar-expand-sm navbar-light bg-primary">
            <div class="container-fluid">
                <div class="collapse navbar-collapse" id="navbarID">
                    <div class="navbar-nav">
                        <Link to="/welcome" className="nav-link active" > Home</Link>
                        <Link to="/products" className="nav-link active"> Products</Link>
                        <Link to="/addproduct" className="nav-link active"> Add Product</Link>
                    </div>
                </div>
            </div>
        </nav>
    </div>
}