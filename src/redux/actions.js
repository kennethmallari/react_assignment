export function removeProduct(index) {
  // action creator
  return {
    type: "REMOVE_PRODUCT",
    index: index,
  };
}

export function addProduct(product) {
  return {
    type: "ADD_PRODUCT",
    product: product,
  };
}

export function loadProduct(products) {
  return {
    type: "LOAD_PRODUCT",
    products: products,
  };
}
