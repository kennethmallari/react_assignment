import _products from "../data/products";

const productReducer = function (state = _products, action) {
  // In Redux state we have products
  switch (action.type) {
    case "REMOVE_PRODUCT":
      return state; // later we can add logic to remove product here

    case "ADD_PRODUCT":
      return state; // later we can add logic to add product here

    case "LOAD_PRODUCTS":
      return state;

    default:
      return state;
  }
};
export default productReducer;
