export default function Product(props) { // this function will receive props onProductRemoved
    console.log("props on product", props);
    return (
        <div>
            <span><img width="50" height="50" src={props.product.imageUrl} /></span>
            <span>{props.product.productName}</span>
            <button type="button" className="btn btn-primary" onClick={() => props.onProductRemoved(props.product)}>Remove</button>
            <span className="fa fa-star"></span>
        </div>
    )
}



