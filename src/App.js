import Main from "./Main";
import { connect } from "react-redux";
import { withRouter } from "react-router";

function mapStateToProps(state) {
  // Mapping of Redux state products to React props
  return {
    products: state,
  };
}

const App = withRouter(connect(mapStateToProps)(Main));
export default App;
