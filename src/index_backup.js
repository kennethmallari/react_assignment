import React from 'react';
import ReactDOM from 'react-dom';

const tasks = ["first", "second", "third"]; // array

// Without JSX 
const element = React.createElement("h1", {}, "Hello World");

const element2 = React.createElement(
    "ul",
    {},
    tasks.map((task, index) => React.createElement("li", { key: index }, task))
); // nested object


// Using JSX - Javascript XML

const element3 = <h1>Hello World {5 * 5}</h1>;
const element4 = (
    <ul>
        {tasks.map((task, index) => (
            <li key={index}>{task}</li>
        ))}
    </ul>
)

const element5 = <div><p>This is Para 1</p>
    <p>This is Para 2</p></div>;

const name = "Justine";
let message = "Hello";
if (name == "ustine") {
    message = "My name is Justine";
} else {
    message = "My name is not Justine"
}

const element6 = <h1>{name == "Jutine" ? "My name is Justine" : "My nams is not Justine"}</h1>;

ReactDOM.render(element6, document.getElementById("root"));
