import React from 'react';
import Main from './Main';
export default class Addproduct extends React.Component {
    constructor(props) {
        super(props);
    }

    handleSubmit = (event) => {
        event.preventDefault();
        const imageUrl = event.target.elements.link.value; // get image url value
        const description = event.target.elements.description.value; // get description value 

        const product = { // creating object which contains all
            productId: Number(new Date()),
            description: description,
            imageUrl: imageUrl
        }

        this.props.onAddProduct(product); // calling props product method
    }

    componentWillUnmount() {
        alert("Component is unmount");
    }

    render() {
        return (
            <div className="m-3">
                <form onSubmit={this.handleSubmit}>
                    <input type="text" name="link" placeholder="Link" />
                    <input type="text" name="description" placeholder="Description" />
                    <button className="btn btn-primary">Add</button>
                </form>
            </div>
        )
    }
}
