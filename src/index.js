import ReactDOM from "react-dom";
import "bootstrap/dist/css/bootstrap.css";
import "font-awesome/css/font-awesome.css";
import App from "./App";
import { BrowserRouter } from "react-router-dom";
import { createStore } from "redux";
import productReducer from "./redux/reducers";
import { Provider } from "react-redux";

const store = createStore(
  productReducer,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);

ReactDOM.render(
  <Provider store={store}>
    <BrowserRouter>
      <App />
    </BrowserRouter>
  </Provider>,
  document.getElementById("root")
);
// import ReactDOM from "react-dom";
// import "bootstrap/dist/css/bootstrap.css";
// import "font-awesome/css/font-awesome.css";
// import App from "./App"; // include App component
// import { BrowserRouter } from "react-router-dom";
// import { createStore } from "redux";
// import productReducer from "./redux/reducers";
// import { Provider } from "react-redux";

// const store = createStore(
//   productReducer,
//   window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
// );

// ReactDOM.render(
//   <Provider store={store}>
//     <BrowserRouter>
//       <App />
//     </BrowserRouter>
//   </Provider>,
//   document.getElementById("root")
// );
