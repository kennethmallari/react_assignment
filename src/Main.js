import React from "react";
import Nav from "./Nav"; // include functional component
import Productlist from "./Productlist";
import AddProduct from "./AddProduct";
import { Route } from "react-router-dom";
import Welcome from "./Welcome";
import { removeProduct } from "./redux/actions";
import { addProduct } from "./redux/actions";

export default class Main extends React.Component {
  // export Main Component
  constructor(props) {
    super(props);
    this.state = {
      // object, initial value we have defined here
      products: [], // empty products inside state
      address: "India",
      laptop: "Macbook",
      username: "Gerald",
      show: true,
    };
    console.log("constructor");
  }

  updateAddress = () => {
    // method
    this.setState({
      address: "Philippines",
    });
  };

  addProduct = (productSubmitted) => {
    // this method will update state products
    debugger;
    this.setState((state) => ({
      // to update state we use setState
      products: state.products.concat([productSubmitted]),
    }));
  };

  removeProduct = (productRemoved) => {
    // remove product and update state
    this.setState((state) => ({
      products: state.products.filter((product) => product != productRemoved),
    }));
  };

  static getDerivedStateFromProps(props, state) {
    // lifecycle update state from props
    return { laptop: props.laptop }; // udpate state
  }

  componentDidMount() {
    // this will call after render method
    console.log("componentDidMount");
    /*
    const data = getDataFromDatabase(); // taking products data
    this.setState({
      // updating state
      products: data,
      username: "Adrian",
    });

    */
    this.props.dispatch(removeProduct(1));
    this.props.dispatch(addProduct({ productName: "Garden Cart" }));
    //this.props.dispatch(addProduct("productName", "Garden Cart"));
  }

  shouldComponentUpdate() {
    return true; // don't update component
  }

  getSnapshotBeforeUpdate(prevProps, prevState) {
    //  document.getElementById("div1").innerHTML = "Before update " + prevState.username;
    return true;
  }

  componentDidUpdate() {
    //   document.getElementById("div2").innerHTML = "After update " + this.state.username;
  }

  deleteAddProduct = () => {
    this.setState({
      show: false,
    });
  };

  render() {
    // life cycle
    console.log("state", this.state);
    console.log("props", this.props);
    console.log("render");
    let addProduct;
    if (this.state.show) {
      addProduct = (
        <AddProduct
          onAddProduct={(addedproduct) => this.addProduct(addedproduct)}
        />
      );
    }
    return (
      <div>
        <Route path="/" component={Nav} />

        <Route path="/addproduct" render={() => addProduct} />

        <Route
          path="/products"
          render={() => (
            <div className="m-3">
              <Productlist products={this.props.products} />
            </div>
          )}
        />

        <Route path="/welcome" component={Welcome} />

        <Route
          path="/extra"
          render={
            <div>
              <div id="div1"></div>
              <div id="div2"></div>

              <button
                type="button"
                className="btn btn-secondary"
                onClick={this.deleteAddProduct}
              >
                Delete Add Product
              </button>
            </div>
          }
        />
      </div>
    );
  }
}
